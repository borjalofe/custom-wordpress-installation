<?php
/**
 * Crea el contenido inicial para un sitio recién instalado.
 *
 * Añade la categoría por defecto "General" (no me gusta "Sin Categoría"),
 * las primeras páginas (por ejemplo, la de "Política de Privacidad"),
 * y widgets por defecto para el tema por defecto de la versión actual.
 *
 * @since 2.1.0
 *
 * @global wpdb       $wpdb
 * @global WP_Rewrite $wp_rewrite
 * @global string     $table_prefix
 *
 * @param int $user_id User ID.
 */
function wp_install_defaults( $user_id ) {
	global $wpdb, $wp_rewrite, $table_prefix;


	// Por defecto, la primera categoría tendrá identificador 1

	$cat_id = 1;
	
	
	/**
	 * Crear categoría
	 *
	 * Esta parte la copiaremos y modificaremos por cada categoría que queramos
	 * crear automáticamente
	 */
	 
	$cat_name = __( 'General' );
	$cat_slug = sanitize_title( _x( 'general', 'Slug de la categoría por defecto' ) );


	// En caso de ser un multisite, la primera categoría no será la 1
	// así que necesitamos saber cuál es su identificador
	
	if ( global_terms_enabled() ) {
			$cat_id = $wpdb->get_var( $wpdb->prepare( "SELECT cat_ID FROM {$wpdb->sitecategories} WHERE category_nicename = %s", $cat_slug ) );
			if ( $cat_id == null ) {
					$wpdb->insert(
							$wpdb->sitecategories,
							array(
									'cat_ID'            => 0,
									'cat_name'          => $cat_name,
									'category_nicename' => $cat_slug,
									'last_updated'      => current_time( 'mysql', true ),
							)
					);
					$cat_id = $wpdb->insert_id;
			}
			
			
			// Si la categoría que estamos creando va a ser la categoría por
			// defecto, necesitamos usar esta línea
			
			update_option( 'default_category', $cat_id );			
	}
			
			
	// Si la categoría que estamos creando va a ser la categoría por
	// defecto, necesitamos usar esta línea, que puede estar fuera
	// del bloque de categorías para multisite
	
	// update_option( 'default_category', $cat_id );
	
	
	// Necesitamos actualizar los términos y taxonomías puesto que una
	// categoría es una taxonomía del blog

	$wpdb->insert(
			$wpdb->terms,
			array(
					'term_id'    => $cat_id,
					'name'       => $cat_name,
					'slug'       => $cat_slug,
					'term_group' => 0,
			)
	);
	$wpdb->insert(
			$wpdb->term_taxonomy,
			array(
					'term_id'     => $cat_id,
					'taxonomy'    => 'category',
					'description' => '',
					'parent'      => 0,
					'count'       => 1,
			)
	);
	
	
	// Si vamos a crear posts para esta categoría, debemos guardar el
	// identificador de la misma para utilizarlo después
	
	$cat_tt_id = $wpdb->insert_id;
	
	
	// Actualizamos el identificador de categorías para utilizarlo a la hora
	// de crear las siguientes categorías
	
	$cat_id ++;
	
	/** Fin de la configuración de UNA categoría */
	
	
	// Todos los contenidos (posts, páginas y custom post types -recuerda
	// que los formularios, imágenes, audios, etc. se guardan como custom
	// post types-) tienen identificadores de post.
	//
	// Aquí establecemos el valor por defecto (1)
	$content_id = 1;
	
	
	// Todos los contenidos tienen fecha de creación y modificación en dos
	// formatos, el "normal" y el "gmt". Ya que estamos creando todo
	// "de golpe", establecemos los valores aquí para utilizarlos a partir de
	// ahora.
	$now             = current_time( 'mysql' );
	$now_gmt         = current_time( 'mysql', 1 );
	
	
	/**
	 * Crear un post
	 *
	 * Esta parte la copiaremos y modificaremos por cada post que queramos
	 * crear automáticamente. Está comentado porque no soy partidario de crear
	 * posts de manera automática en un sitio nuevo.
	 */
	 
	/*$first_post_guid = get_option( 'home' ) . '/?p=' . $content_id;

	if ( is_multisite() ) {
        $first_post = get_site_option( 'first_post' );

        if ( ! $first_post ) {
            $first_post = "<!-- wp:paragraph -->\n<p>" .
            // translators: first post content, %s: site link
            __( 'Welcome to %s. This is your first post. Edit or delete it, then start writing!' ) .
            "</p>\n<!-- /wp:paragraph -->";
        }

        $first_post = sprintf(
            $first_post,
            sprintf( '<a href="%s">%s</a>', esc_url( network_home_url() ), get_network()->site_name )
        );

        // Back-compat for pre-4.4
        $first_post = str_replace( 'SITE_URL', esc_url( network_home_url() ), $first_post );
        $first_post = str_replace( 'SITE_NAME', get_network()->site_name, $first_post );
	} else {
        $first_post = "<!-- wp:paragraph -->\n<p>" .
        // translators: first post content, %s: site link
        __( 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!' ) .
        "</p>\n<!-- /wp:paragraph -->";
	}

	$wpdb->insert(
        $wpdb->posts,
        array(
            'post_author'           => $user_id,
            'post_date'             => $now,
            'post_date_gmt'         => $now_gmt,
            'post_content'          => $first_post,
            'post_excerpt'          => '',
            'post_title'            => __( 'Hello world!' ),
            /* translators: Default post slug * /
            'post_name'             => sanitize_title( _x( 'hello-world', 'Default post slug' ) ),
            'post_modified'         => $now,
            'post_modified_gmt'     => $now_gmt,
            'guid'                  => $first_post_guid,
            'comment_count'         => 1,
            'to_ping'               => '',
            'pinged'                => '',
            'post_content_filtered' => '',
        )
	);
	$wpdb->insert(
        $wpdb->term_relationships,
        array(
            'term_taxonomy_id' => $cat_tt_id,
            'object_id'        => 1,
        )
	);

	// Default comment
	if ( is_multisite() ) {
        $first_comment_author = get_site_option( 'first_comment_author' );
        $first_comment_email  = get_site_option( 'first_comment_email' );
        $first_comment_url    = get_site_option( 'first_comment_url', network_home_url() );
        $first_comment        = get_site_option( 'first_comment' );
	}

	$first_comment_author = ! empty( $first_comment_author ) ? $first_comment_author : __( 'A WordPress Commenter' );
	$first_comment_email  = ! empty( $first_comment_email ) ? $first_comment_email : 'wapuu@wordpress.example';
	$first_comment_url    = ! empty( $first_comment_url ) ? $first_comment_url : 'https://wordpress.org/';
	$first_comment        = ! empty( $first_comment ) ? $first_comment : __(
			'Hi, this is a comment.
To get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.
Commenter avatars come from <a href="https://gravatar.com">Gravatar</a>.'
	);
	$wpdb->insert(
        $wpdb->comments,
        array(
            'comment_post_ID'      => 1,
            'comment_author'       => $first_comment_author,
            'comment_author_email' => $first_comment_email,
            'comment_author_url'   => $first_comment_url,
            'comment_date'         => $now,
            'comment_date_gmt'     => $now_gmt,
            'comment_content'      => $first_comment,
        )
	);
	
    $content_id++;
    */
	
	/** Fin de la configuración de UN post */
	
	
	/**
	 * Crear una página
	 *
	 * Esta parte la copiaremos y modificaremos por cada página que queramos
	 * crear automáticamente.
	 */
	 
	
	// Contenido de la página (a pelo). Fíjate que este contenido incluye
	// bloques de párrafo (<!-- wp:paragraph --> y <!-- /wp:paragraph -->)
	// y citas (<!-- wp:quote --> y <!-- /wp:quote -->), pero podría llevar
	// otros (aunque necesitaremos ver qué códigos se utilizan, especialmente
	// para el caso de constructores visuales como Elementor o Beaver Builder).
	
	$first_page = "<!-- wp:paragraph -->\n<p>";
	/* translators: first page content */
	$first_page .= __( "This is an example page. It's different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:" );
	$first_page .= "</p>\n<!-- /wp:paragraph -->\n\n";

	$first_page .= "<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>";
	/* translators: first page content */
	$first_page .= __( "Hi there! I'm a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin' caught in the rain.)" );
	$first_page .= "</p></blockquote>\n<!-- /wp:quote -->\n\n";

	$first_page .= "<!-- wp:paragraph -->\n<p>";
	/* translators: first page content */
	$first_page .= __( '...or something like this:' );
	$first_page .= "</p>\n<!-- /wp:paragraph -->\n\n";

	$first_page .= "<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>";
	/* translators: first page content */
	$first_page .= __( 'The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.' );
	$first_page .= "</p></blockquote>\n<!-- /wp:quote -->\n\n";

	$first_page .= "<!-- wp:paragraph -->\n<p>";
	$first_page .= sprintf(
			/* translators: first page content, %s: site admin URL */
			__( 'As a new WordPress user, you should go to <a href="%s">your dashboard</a> to delete this page and create new pages for your content. Have fun!' ),
			admin_url()
	);
	$first_page .= "</p>\n<!-- /wp:paragraph -->";
	
	
	// Contenido de la página (desde un archivo -OJO, tendrás que incluirlo-)
	
	//$first_page = file_exists( WP_CONTENT_DIR . '/first_page.txt') ? file_get_contents( 'first_page.txt', true ) : '';
	

	$first_post_guid = get_option( 'home' ) . '/?page_id=' . $content_id;
	
	
	// Aquí es donde realmente creamos la página
	
	$wpdb->insert(
			$wpdb->posts,
			array(
					'post_author'           => $user_id,
					'post_date'             => $now,
					'post_date_gmt'         => $now_gmt,
					'post_content'          => $first_page,
					'post_excerpt'          => '',
					'comment_status'        => 'closed',
					'post_title'            => __( 'Sample Page' ),
					/* translators: Default page slug */
					'post_name'             => __( 'sample-page' ),
					'post_modified'         => $now,
					'post_modified_gmt'     => $now_gmt,
					'guid'                  => $first_post_guid,
					'post_type'             => 'page',
					'to_ping'               => '',
					'pinged'                => '',
					'post_content_filtered' => '',
			)
	);

	// Aquí recuperamos el identificador de la página para usarlo después
	// por ejemplo, para establecer que la página de inicio es una página
	// concreta y la página de blog es otra diferente
	$first_page_id = $wpdb->insert_id;


    /*update_option( 'show_on_front', 'page' ); 
    update_option( 'page_on_front', $first_page_id );
    update_option( 'page_for_posts', $other_page_id );*/
	
	
	// Y aquí es donde le asignamos la plantilla
	
	$wpdb->insert(
			$wpdb->postmeta,
			array(
					'post_id'    => $content_id,
					'meta_key'   => '_wp_page_template',
					'meta_value' => 'default',
			)
	);
	
	$content_id++;
	
	/** Fin de la configuración de UNA página */
	
	
	/**
	 * Crear la página de "Política de Privacidad"
	 *
	 * Con RGPD, Automattic ha incluido facilidades para crear la página de la
	 * política de privacidad. He comentado la generación por parte de
	 * Automattic porque yo prefiero incluir mi propio texto.
	 */
	 
	/*if ( is_multisite() ) {
	
	
		// En multisite, puesto que ya "debe" existir, recuperamos el contenido
		// para utilizarlo en el nuevo sitio.
		//
		// OJO: si los sitios tienen diferente URL o dueño, tendremos que
		// modificar los datos de esta página.
		
		$privacy_policy_content = get_site_option( 'default_privacy_policy_content' );
		
	} else {
	
	
		// Si no es multisite, asumimos que no existe el contenido y tratamos
		// de recuperarlo desde la ayuda que nos proporciona WordPress.
		//
		// OJO: tendremos que modificar los datos de esta página para que se
		// ajuste a los valores reales.
		
		if ( ! class_exists( 'WP_Privacy_Policy_Content' ) ) {
				include_once( ABSPATH . 'wp-admin/includes/misc.php' );
		}

		$privacy_policy_content = WP_Privacy_Policy_Content::get_default_content();
	}*/
	
	
	// Recupero el contenido de base que he incluido en la carpeta de instalación
	
	$privacy_policy_content = file_exists( WP_CONTENT_DIR . '/privacidad.txt') ? file_get_contents( 'privacidad.txt', true ) : '';
	
	
	if ( ! empty( $privacy_policy_content ) ) {
		$privacy_policy_guid = get_option( 'home' ) . '/?page_id=' . $content_id;

		$wpdb->insert(
			$wpdb->posts,
			array(
				'post_author'           => $user_id,
				'post_date'             => $now,
				'post_date_gmt'         => $now_gmt,
				'post_content'          => $privacy_policy_content,
				'post_excerpt'          => '',
				'comment_status'        => 'closed',
				'post_title'            => __( 'Privacy Policy' ),
				/* translators: Privacy Policy page slug */
				'post_name'             => __( 'privacy-policy' ),
				'post_modified'         => $now,
				'post_modified_gmt'     => $now_gmt,
				'guid'                  => $privacy_policy_guid,
				'post_type'             => 'page',
				'post_status'           => 'draft',
				'to_ping'               => '',
				'pinged'                => '',
				'post_content_filtered' => '',
			)
		);
		$wpdb->insert(
			$wpdb->postmeta,
			array(
				'post_id'    => $content_id,
				'meta_key'   => '_wp_page_template',
				'meta_value' => 'default',
			)
		);
		update_option( 'wp_page_for_privacy_policy', $content_id );
	}
	
	$content_id++;
	
	/** Fin de la configuración de la página de privacidad */
	
	
	/**
	 * Configurar los widgets de un tema (en este caso, del tema por defecto).
	 */
	 
	update_option(
		'widget_search',
		array(
				2              => array( 'title' => '' ),
				'_multiwidget' => 1,
		)
	);
	update_option(
		'widget_recent-posts',
		array(
			2              => array(
				'title'  => '',
				'number' => 5,
			),
			'_multiwidget' => 1,
		)
	);
	update_option(
		'widget_recent-comments',
		array(
			2              => array(
				'title'  => '',
				'number' => 5,
			),
			'_multiwidget' => 1,
		)
	);
	update_option(
		'widget_archives',
		array(
			2              => array(
				'title'    => '',
				'count'    => 0,
				'dropdown' => 0,
			),
			'_multiwidget' => 1,
		)
	);
	update_option(
		'widget_categories',
		array(
			2              => array(
				'title'        => '',
				'count'        => 0,
				'hierarchical' => 0,
				'dropdown'     => 0,
			),
			'_multiwidget' => 1,
		)
	);
	update_option(
		'widget_meta',
		array(
			2              => array( 'title' => '' ),
			'_multiwidget' => 1,
		)
	);
	update_option(
		'sidebars_widgets',
		array(
			'wp_inactive_widgets' => array(),
			'sidebar-1'           => array(
				0 => 'search-2',
				1 => 'recent-posts-2',
				2 => 'recent-comments-2',
				3 => 'archives-2',
				4 => 'categories-2',
				5 => 'meta-2',
			),
			'array_version'       => 3,
		)
	);
	
	/** Fin de la configuración de los widgets */
	
	

	/**
	 * Establecer las opciones relacionadas con el usuario principal
	 */
	
	if ( ! is_multisite() ) {
		update_user_meta( $user_id, 'show_welcome_panel', 1 );
	} elseif ( ! is_super_admin( $user_id ) && ! metadata_exists( 'user', $user_id, 'show_welcome_panel' ) ) {
		update_user_meta( $user_id, 'show_welcome_panel', 2 );
	}

	if ( is_multisite() ) {
		// Flush rules to pick up the new page.
		$wp_rewrite->init();
		$wp_rewrite->flush_rules();

		$user = new WP_User( $user_id );
		$wpdb->update( $wpdb->options, array( 'option_value' => $user->user_email ), array( 'option_name' => 'admin_email' ) );

		// Remove all perms except for the login user.
		$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->usermeta WHERE user_id != %d AND meta_key = %s", $user_id, $table_prefix . 'user_level' ) );
		$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->usermeta WHERE user_id != %d AND meta_key = %s", $user_id, $table_prefix . 'capabilities' ) );

		// Delete any caps that snuck into the previously active blog. (Hardcoded to blog 1 for now.) TODO: Get previous_blog_id.
		if ( ! is_super_admin( $user_id ) && $user_id != 1 ) {
			$wpdb->delete(
				$wpdb->usermeta,
				array(
					'user_id'  => $user_id,
					'meta_key' => $wpdb->base_prefix . '1_capabilities',
				)
			);
		}
	}
	
	/** Fin de la configuración de las opciones relacionadas con el usuario principal */
	
	

	/**
	 * Configurar los permalinks
	 */
	 
    update_option( 'selection', 'custom' ); 
    update_option( 'permalink_structure', '/%postname%/' ); 
    $wp_rewrite->init();
    $wp_rewrite->flush_rules(); 
	
	/** Fin de la configuración de los permalinks */
	
	

	/**
	 * Configuración de opciones
	 */
	
	update_option( 'date_format', 'd/m/Y' );
	update_option( 'links_updated_date_format', 'd/m/Y H:i' );
    update_option( 'start_of_week', 1 ); 
	update_option( 'time_format', 'H:i' );
	update_option( 'timezone_string', 'Europe/Madrid' );
    update_option( 'uploads_use_yearmonth_folders', 0 );
	update_option( 'use_smilies', 0 );
	update_option( 'WPLANG', 'es_ES' );
	
	/** Fin de la configuración de las opciones relacionadas con el usuario principal */
}