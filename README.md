# Custom WordPress Installation

Everyone of us install a _WordPress_ site the same way everytime we do it:

* same plugin's core
* almost same theme
* same basic settings

So, why don't we add that kind of stuff into the _WordPress_ installation process?